//
//  MainViewController.swift
//  N Plus One
//
//  Created by Macbook on 6/23/17.
//  Copyright © 2017 MyFirstApp. All rights reserved.
//

import UIKit
import MBProgressHUD

class MainViewController: UIViewController {

    @IBOutlet weak var inputField: UITextField?
    @IBOutlet weak var numbersLabel: UILabel?
    @IBOutlet weak var scoreLabel: UILabel?
    @IBOutlet weak var timerLabel: UILabel?
    var score: Int = 0
    var hud:MBProgressHUD?
    var timer:Timer?
    var seconds:Int = 130
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        hud = MBProgressHUD(view: self.view)
        if hud != nil {
            self.view.addSubview(hud!)
        }
        // Do any additional setup after loading the view.
        setRandomNumberlabel()
        updateScoreLabel()
        
        inputField?.addTarget(self, action: #selector(textFieldDidChange(textField:)), for:UIControlEvents.editingChanged)
    }
//    @IBAction func textFieldDidChange(_ sender: Any) {
//    }
    
    func showHUDWithAnswer(isRight:Bool)
    {
        var imageView:UIImageView?
        
        if isRight
        {
            imageView = UIImageView(image: UIImage(named:"thumbs-up"))
        }
        else
        {
            imageView = UIImageView(image: UIImage(named:"thumbs-down"))
        }
        
        if(imageView != nil)
        {
            hud?.mode = MBProgressHUDMode.customView
            hud?.customView = imageView
            
            hud?.show(animated: true)
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                self.hud?.hide(animated: true)
                self.inputField?.text = ""
            }
        }
    }

    func textFieldDidChange(textField: UITextField) {
        if textField.text?.characters.count ?? 0 < 4 {
            return
        } else if (textField.text?.characters.count ?? 0 > 4) {
            print("no..")
            inputField?.text = ""
            return
        }
        
        if let numbers_text = numbersLabel?.text,
            let input_text = inputField?.text,
            let numbers = Int(numbers_text),
            let input = Int(input_text)
            
        {
             print("Comparing: \(input_text) minus \(numbers_text) == \(input - numbers)")
            
            var isCorrect = true;
            
            print("numbers: \(numbers)");
            print("input: \(input)");
            for index in 0...3 {
                let digit = Int(pow(Double(10), Double(index)))
                print("digit: \(digit)");

                let numDigit = (numbers % (digit*10)) / digit
                let inputDigit = (input % (digit*10)) / digit
                print("numDigit: \(numDigit)")
                print("inputDigit: \(inputDigit)")
                if (numDigit+1 != inputDigit && !(numDigit == 9 && inputDigit==0)) {
                    isCorrect = false
                    break
                }
            }
            
            if(isCorrect)
            {
                print("Correct!")
                
                score += 1
                showHUDWithAnswer(isRight: true)
            }
            else
            {
                showHUDWithAnswer(isRight: false)
                print("Incorrect!")
                if (score > 0) {
                    score -= 1
                    
                }
            }
//            inputField?.text = ""
            setRandomNumberlabel()
            
            if (timer == nil) {
                timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector:#selector(onUpdateTimer), userInfo:nil, repeats:true)
            }
            updateScoreLabel()
        }
    }
    func onUpdateTimer() -> Void {
        
        if seconds > 0 && score > 10 {
            seconds -= 2
            updateTimerLabel()
            
        } else if (seconds > 0) {
            seconds -= 1
            updateTimerLabel()
            
        } else if (seconds == 0) {
            if (timer != nil) {
                timer!.invalidate()
                timer = nil
                
                
                let alertController = UIAlertController(title: "Time Up!", message: "Your time is up! You got a score of: \(score) points. Very good!", preferredStyle: .alert)
                
                let restartAction = UIAlertAction(title: "Restart", style: .default, handler: nil)
                alertController.addAction(restartAction)
                
                self.present(alertController, animated: true, completion: nil)
                
                score = 0
                seconds = 120
                
                updateTimerLabel()
                updateScoreLabel()
                setRandomNumberlabel()
            }
            
        }
    }
    func updateTimerLabel()
    {
        if(timerLabel != nil)
        {
            let min:Int = (seconds / 60) % 60
            let sec:Int = seconds % 60
            
            let min_p:String = String(format: "%02d", min)
            let sec_p:String = String(format: "%02d", sec)
            
            timerLabel!.text = "\(min_p):\(sec_p)"
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    func updateScoreLabel() {
        scoreLabel?.text = "\(score)"
    }
    
    func setRandomNumberlabel() {
        numbersLabel?.text = generateRandomNumber()
    }
    
    func generateRandomNumber() -> String {
        var result:String = ""
        for _ in 1...4 {
            let digit:Int = Int(arc4random_uniform(10))
            result += "\(digit)"
        }
        
        return result
    }

}
